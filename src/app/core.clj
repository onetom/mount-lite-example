(ns app.core
  (:gen-class)
  (:require [clojure.tools.namespace.repl]
            [mount.lite :as mount]
            [mount.extensions.refresh :refer [refresh]]
            [app.db]))

(defn -main [& args]
  (println "Hello, mount-lite!")
  (mount/start)
  (mount/status))

(comment
  (refresh)
  (-main)
  (mount/start)
  @app.db/conn
  (mount/status)
  (mount/stop))
