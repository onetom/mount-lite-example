(ns app.schema
  (:require [mount.lite :refer [defstate] :as mount]))

(def default-schema
  [{:db/ident       :str
    :db/valueType   :db.type/string
    :db/cardinality :db.cardinality/one}])

(defstate schema :start default-schema)
