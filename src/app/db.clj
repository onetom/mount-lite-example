(ns app.db
  (:require [mount.lite :refer [defstate] :as mount]
            [app.config :refer [config]]
            [app.schema :refer [schema]]
            [datomic.api :as d]
            [clojure.string :as str]))

(defstate conn
  :start (d/connect (-> @config :datomic :uri))
  :stop (d/release @conn)
  :dependencies #{#'config})

(defn tx! [tx-data] @(d/transact @conn tx-data))
(defn db [] (d/db @conn))
(defn q [query & inputs] (apply d/q query (db) inputs))

(def test-uri "datomic:mem://test")

(defn connect-in-memory-db [uri]
  (println "\n===>>> Setting up test DB <<<===\n")
  (if (str/starts-with? uri "datomic:mem://")
    (do (d/delete-database uri)
        (d/create-database uri)
        (let [conn' (d/connect uri)]
          @(d/transact conn' @schema)
          conn'))
    (throw (str (pr-str uri) " is not an in-memory database."))))

(defn cleanup-in-memory-db [uri conn]
  (println "\n===>>> Tearing down test DB <<<===\n")
  (try (d/release conn))
  (when (str/starts-with? uri "datomic:mem://")
    (d/delete-database uri)))

(defn test-conn []
  (mount/state :start (connect-in-memory-db test-uri)
               :stop (cleanup-in-memory-db test-uri "<how can i get the conn???>")))
