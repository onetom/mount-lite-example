(ns app.config
  (:require [mount.lite :refer [defstate] :as mount]))

(defstate config
  :start {:env     :dev
          :datomic {:uri "datomic:mem://dev"}})
