(ns app.db-test
  (:use debux.core)
  (:require [clojure.test :refer :all])
  (:require [mount.lite :as mount]
            [app.db :refer [conn test-conn tx! db q]]))

(deftest test-conn-test
    (let [res (-> (mount/with-session
                    (mount/with-substitutes
                      [#'conn (test-conn)]
                      (mount/start))
                    (tx! [{:db/id "e" :str "asd"}])
                    (q '[:find (pull ?e [:str]) .
                         :where [?e :str "asd"]]))
                  :result deref)]
      (is (= {:str "asd"} res))))
