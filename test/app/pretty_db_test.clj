(ns app.pretty-db-test
  (:use debux.core)
  (:require [clojure.test :refer :all])
  (:require [mount.lite :as mount]
            [app.db :refer [conn test-conn tx! db q]]))

(defn with-test-db [tests]
  (-> (mount/with-session
        (mount/with-substitutes
          [#'conn (test-conn)]
          (mount/start))
        (tests))
      :result deref))

(use-fixtures :each with-test-db)

(deftest test-inside-session-using-fixtures
  (tx! [{:db/id "e" :str "asd"}])
  (is (= {:str "asd"}
         (q '[:find (pull ?e [:str]) .
              :where [?e :str "asd"]]))))
