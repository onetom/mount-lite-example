(defproject app "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/tools.namespace "0.2.11"]
                 [philoskim/debux "0.3.4"]
                 [functionalbytes/mount-lite "2.0.0-SNAPSHOT"]
                 [com.datomic/datomic-free "0.9.5561.50"]]
  :main ^:skip-aot app.core
  :jvm-opts ["-XX:+TieredCompilation" "-XX:TieredStopAtLevel=1" "-Xverify:none"]
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
